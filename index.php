<?php

function getVariableTypeThroughIfElse($variable)
{
    if (is_bool($variable)) {
        echo '_bool_' . PHP_EOL;
    } elseif (is_float($variable)) {
        echo '_float_' . PHP_EOL;
    } elseif (is_int($variable)) {
        echo '_int_' . PHP_EOL;
    } elseif (is_string($variable)) {
        echo '_string_' . PHP_EOL;
    } elseif (is_null($variable)) {
        echo '_null_' . PHP_EOL;
    } else {
        echo '_other_' . PHP_EOL;
    }
}

function getVariableTypeThroughSwitchCase($variable)
{
    switch (gettype($variable)) {
        case "boolean":
            echo '_bool_' . PHP_EOL;
            break;
        case "integer":
            echo '_int_' . PHP_EOL;
            break;
        case "double":
            echo '_float_' . PHP_EOL;
            break;
        case "string":
            echo '_string_' . PHP_EOL;
            break;
        case "NULL":
            echo '_null_' . PHP_EOL;
            break;
        default:
            echo '_other_' . PHP_EOL;
    }
}

$array = [1, 'one', true, 3.14, null];

foreach ($array as $value) {
    $outVariable = is_bool($value)
        ? ($value ? 'true' : 'false')
        : (is_null($value) ? 'null' : $value);

    echo "Переменная $outVariable имеет тип: ";
    getVariableTypeThroughIfElse($value);

    echo "Переменная $outVariable имеет тип: ";
    getVariableTypeThroughSwitchCase($value);
}